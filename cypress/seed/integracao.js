import monica from '@softplan/monica';
const Configuration = require('../../configuration/project/configuration');

export default class integracao {
  constructor() {
    monica.usaConfig(`./configuration/project/config.${Configuration.environments.NODE_ENV}.json`);
    monica.info();
  }

  criaDistribuiProcesso(user) {
    return browser.call(() => monica.geraToken(user, 'agesune1').then(token => 
      monica.criaDistribuiProcesso(token).then(
        processo => processo)));
  }
}