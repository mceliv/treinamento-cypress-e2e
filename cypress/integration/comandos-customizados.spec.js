describe("Tickets", ()=>{

    beforeEach(()=> cy.visit("https://bit.ly/2XSuwCW"))

    it("Preenche campos obrigatórios usando o comando support", () =>{
        const customer = {
            firstName: "Monica",
            lastName: "Eli",
            email: "teste@teste.com.br"
        }

        cy.fillMandatoryFields(customer);
        
        cy.get("button[type='submit']").should("not.be.disabled");

        cy.get("#agree").uncheck();
        cy.get("button[type='submit']").should("be.disabled");

    });
})