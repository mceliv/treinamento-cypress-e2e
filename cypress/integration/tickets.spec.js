describe("Tickets", ()=>{

    beforeEach(()=> cy.visit("https://bit.ly/2XSuwCW"))

    it("Interagindo com campos do tipo Input", () =>{ //caso coloque o .only irá executar somente esse teste
        const firstName = "Monica";
        const lastName  = "Eli";

        cy.get("#first-name").type(firstName); //.type (digitar)
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("teste@teste.com.br");
        cy.get("#requests").type("Vegetarian");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    it("Interagindo com campos do tipo select", () =>{
        cy.get("#ticket-quantity").select("2"); //.select são para selecionar um campo do tipo select
    })

    it("Interagindo com radioButtons",() => {
        cy.get("#vip").check();        
    });

    it("Interagindo com checkbox", () => {
        cy.get('#friend').check();
        cy.get('#publication').check();
        cy.get('#social-media').check();
    });

    it("Marcar Friend e Publication, então desmarcar 'Friend'", () => {
        cy.get('#friend').check();
        cy.get('#publication').check();
        cy.get('#friend').uncheck(); //Desmarcar um checkbox
    });

    it("Realizando assertions", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    
    it("Realizando assertions de e-mail invalido", () => {
        cy.get("#email").type("teste-gmail.com.br");

        cy.get("#email.invalid").should("exist");
    });

    it("Dando apelido para seletores comuns 'Alias'", () => {
        cy.get("#email")
            .as("email")//Apelido (Alias) do #email
            .type("teste-gmail.com.br");

        cy.get("#email.invalid").should("exist");

        cy.get("@email") // Usando o apelido (Alias)
            .clear() //Limpar o campo
            .type("teste@SpeechGrammarList.com");

        cy.get("#email.invalid")
            .should("not.exist")
    });
})