describe("Tickets", ()=>{

    beforeEach(()=> cy.visit("https://bit.ly/2XSuwCW"))

    it("Preenche e redefine o formulário", () =>{
        const firstName = "Monica";
        const lastName  = "Eli";

        const fullName = `${firstName} ${lastName}`

        cy.get("#first-name").type(firstName); //Digita primeiro nome
        cy.get("#last-name").type(lastName); //Digita segundo nome
        cy.get("#email").type("teste@teste.com.br"); //Digita e-mail

        cy.get("#ticket-quantity").select("2"); //Seleciona duas unidades de ticket
        cy.get("#vip").check(); //Seleciona o ticket VIP
        cy.get('#friend').check(); // Seleciona que ficou sabendo por um amigo
        cy.get("#requests").type("IPA beer");

        cy.get(".agreement p").should(
            "contain", 
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        );
        
        cy.get("#agree").click();
        cy.get("#signature").type(fullName);
        cy.get("button[type='submit']").should("not.be.disabled");

        cy.get("button[type='reset']").click();
        cy.get("button[type='submit']").should("be.disabled");
    });
})